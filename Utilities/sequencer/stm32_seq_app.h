/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32_SEQ_APP_H__
#define __STM32_SEQ_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32_seq.h"

typedef enum
{
  CFG_TASK_TEST1_ID,
  CFG_TASK_TEST2_ID,
  CFG_TASK_TEST3_ID,
	
} CFG_Task_Id_t;

void UTIL_SEQ_APP_Init(void);

void Test1(void);
void Test2(void);
void Test3(void);


#ifdef __cplusplus
}
#endif

#endif

