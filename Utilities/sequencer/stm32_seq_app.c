/* Includes ------------------------------------------------------------------*/
#include "stm32_seq_app.h"
#include "stm32_uart.h"
#include "stm32_tim.h"


void UTIL_SEQ_APP_Init(void)
{
  UTIL_SEQ_RegTask(1 << CFG_TASK_TEST1_ID, UTIL_SEQ_RFU, Test1);
  UTIL_SEQ_RegTask(1 << CFG_TASK_TEST2_ID, UTIL_SEQ_RFU, Test2);
  UTIL_SEQ_RegTask(1 << CFG_TASK_TEST3_ID, UTIL_SEQ_RFU, Test3);
}


void Test1(void)
{
  /* 复制到发送缓存区 */
  memcpy(UART1_Transfer.TxData, UART1_Transfer.RxData, UART1_Transfer.RxData_Len);
  UART1_Transfer.TxData_Len = UART1_Transfer.RxData_Len;
  
  /* 清空接收缓存区 */
  memset(UART1_Transfer.RxData, 0, UART1_Transfer.RxData_Len);
  UART1_Transfer.RxData_Len = 0;
  
  STM32_UART_TransmitData(&huart1, UART1_Transfer.TxData, UART1_Transfer.TxData_Len);
}

void Test2(void)
{
  /* 复制到发送缓存区 */
  memcpy(LPUART1_Transfer.TxData, LPUART1_Transfer.RxData, LPUART1_Transfer.RxData_Len);
  LPUART1_Transfer.TxData_Len = LPUART1_Transfer.RxData_Len;
  
  /* 清空接收缓存区 */
  memset(LPUART1_Transfer.RxData, 0, LPUART1_Transfer.RxData_Len);
  LPUART1_Transfer.RxData_Len = 0;
  
  STM32_UART_TransmitData(&hlpuart1, LPUART1_Transfer.TxData, LPUART1_Transfer.TxData_Len);
}

void Test3(void)
{
  printf("Test3\r\n");
}


void UTIL_SEQ_Idle(void)
{
  /* 空 */
}

void UTIL_SEQ_PreIdle(void)
{
  HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
}

void UTIL_SEQ_PostIdle(void)
{
  if (UART1_Transfer.Transfer_RX_Status == UART_TRANSFER_RXEND)
  {
    UTIL_SEQ_SetTask(1 << CFG_TASK_TEST1_ID, 0);
    
    UART1_Transfer.Transfer_RX_Status = UART_TRANSFER_IDLE;
  }
  
  if (LPUART1_Transfer.Transfer_RX_Status == UART_TRANSFER_RXEND)
  {
    UTIL_SEQ_SetTask(1 << CFG_TASK_TEST2_ID, 0);
    
    LPUART1_Transfer.Transfer_RX_Status = UART_TRANSFER_IDLE;
  }
}


