/* Includes ------------------------------------------------------------------*/
#include "software_tim.h"


static volatile uint32_t tickCnt = 0;

static Software_Timer_t timer[SOFTWARE_TIMER_NUM];


void Software_Timer_Update_TickCount(void)
{
  tickCnt++;
}


uint32_t Software_Timer_Get_TickCount(void)
{
  return tickCnt;
}


int32_t Software_Timer_Init(void)
{
  uint16_t i;
  for (i = 0; i < SOFTWARE_TIMER_NUM; i++) 
  {
    timer[i].State = STIM_STATE_STOPPED;
    timer[i].Mode = STIM_MODE_ONESHOT;
    timer[i].Match = 0;
    timer[i].Period = 0;
    timer[i].Callback = NULL;
  }

  return STIM_ERROR_NO;
}


int32_t Software_Timer_Start(uint16_t id, Software_Timer_Mode_t mode, uint32_t period, Software_Timer_Callback *cb)
{
  if (id > SOFTWARE_TIMER_NUM)
  {
    return STIM_ERROR_ID;
  }
  if ((mode != STIM_MODE_ONESHOT) && (mode != STIM_MODE_CONTINUOUS))
  {
    return STIM_ERROR_MODE;
  }

  timer[id].Match = Software_Timer_Get_TickCount() + period;
  timer[id].Period = period;
  timer[id].State = STIM_STATE_RUNNING;
  timer[id].Mode = mode;
  timer[id].Callback = cb;

  return STIM_ERROR_NO;
}


int32_t Software_Timer_Update(void)
{
  uint16_t i;

  for (i = 0; i < SOFTWARE_TIMER_NUM; i++) 
  {
    switch (timer[i].State) 
    {
      case STIM_STATE_STOPPED:
        break;
    
      case STIM_STATE_RUNNING:
        if(timer[i].Match <= Software_Timer_Get_TickCount()) 
        {
          timer[i].State = STIM_STATE_TIMEOUT;
          timer[i].Callback();
        }
        break;
      
      case STIM_STATE_TIMEOUT:
        if(timer[i].Mode == STIM_MODE_ONESHOT) 
        {
          timer[i].State = STIM_STATE_STOPPED;
        } 
        else 
        {
          timer[i].Match = Software_Timer_Get_TickCount() + timer[i].Period;
          timer[i].State = STIM_STATE_RUNNING;
        }
        break;
    
      default:
        printf("timer[%d] state error!\r\n", i);
        break;
    }
  }

  return STIM_ERROR_NO;
}


int32_t Software_Timer_Stop(uint16_t id)
{
  if (id > SOFTWARE_TIMER_NUM)
  {
    return STIM_ERROR_ID;
  }

  timer[id].State = STIM_STATE_STOPPED;

  return STIM_ERROR_NO;
}

int32_t Software_Timer_Get_State(uint16_t id)
{
  return timer[id].State;
}


void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
  if (hlptim->Instance == LPTIM1)
  {
    /* 也可以放空闲任务中 */
    Software_Timer_Update();
    Software_Timer_Update_TickCount();
  }
}


