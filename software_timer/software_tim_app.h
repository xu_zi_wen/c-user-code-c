/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SOFTWARE_TIM_APP_H__
#define __SOFTWARE_TIM_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "software_tim.h"


void Software_Timer_APP_Init(void);

void test_timer(void);


#ifdef __cplusplus
}
#endif

#endif

