/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SOFTWARE_TIM_H__
#define __SOFTWARE_TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "lptim.h"


#define SOFTWARE_TIMER_NUM    16

typedef void Software_Timer_Callback(void);

typedef struct _Software_Timer_t
{
  uint8_t State;
  uint8_t Mode;
  uint32_t Match;
  uint32_t Period;
  Software_Timer_Callback *Callback;
} Software_Timer_t;

typedef enum _Software_Timer_State_t
{
  STIM_STATE_STOPPED,
  STIM_STATE_RUNNING,
  STIM_STATE_TIMEOUT,
} Software_Timer_State_t;

typedef enum _Software_Timer_Mode_t
{
  STIM_MODE_ONESHOT,
  STIM_MODE_CONTINUOUS,
} Software_Timer_Mode_t;

typedef enum _Software_Timer_Error_t
{
  STIM_ERROR_NO,
  STIM_ERROR_ID,
  STIM_ERROR_MODE,
} Software_Timer_Error_t;


void Software_Timer_Update_TickCount(void);
uint32_t Software_Timer_Get_TickCount(void);
int32_t Software_Timer_Init(void);
int32_t Software_Timer_Start(uint16_t id, Software_Timer_Mode_t mode, uint32_t period, Software_Timer_Callback *cb);
int32_t Software_Timer_Update(void);
int32_t Software_Timer_Stop(uint16_t id);
int32_t Software_Timer_Get_State(uint16_t id);

#ifdef __cplusplus
}
#endif

#endif

