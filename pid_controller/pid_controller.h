#ifndef __PI_CONTROLLER_H__
#define __PI_CONTROLLER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <math.h>


typedef struct
{
  float err;
  float ref;
  float in;
  float Up;
  float Ui;
  float Ud;
  float Kp;
  float Ki;
  float Kd;
  float out;
} pid_ctrl_f32_t;

extern pid_ctrl_f32_t pid1;

int32_t pid_controller_init_f32(pid_ctrl_f32_t *pid_ctrl);
int32_t pid_controller_update_f32(pid_ctrl_f32_t *pid_ctrl);




typedef struct 
{
  float Kp;            // 比例系数
  float Ki;            // 积分系数
  float Kd;            // 微分系数
  float integral;      // 积分累积
  float prev_error;    // 上一次误差
  float max_output;    // 输出限幅（如 0~100%）
  float max_integral;  // 积分抗饱和限幅
} pid_instance_f32_t;


int32_t pid_init_f32(pid_instance_f32_t *pid, float Kp, float Ki, float Kd, 
  float max_output, float max_integral);
float pid_update_f32(pid_instance_f32_t *pid, float setpoint, float measurement);


#ifdef __cplusplus
}
#endif

#endif


