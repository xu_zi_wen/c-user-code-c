/* 2024/12/11-09:48:00 V1.1.0.1 */
#ifndef __DEVICE_INFO_H__
#define __DEVICE_INFO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
  

typedef struct _device_info_t
{
  uint32_t product_id;
  uint32_t hardware_version;
  uint32_t software_version;
  uint32_t firmware_version;
  uint32_t number;
  char *serial_number;
  char *description;
} device_info_t;


extern device_info_t device0_info;


#ifdef __cplusplus
}
#endif

#endif


