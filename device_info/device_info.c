/* 2024/12/11-09:48:00 V1.1.0.1 */
#include "device_info.h"


static char device_serial_number[32] = "1234567890";
static char device_description[32] = "Device description";


device_info_t device0_info = 
{
  .product_id = 0x00000001,
  .hardware_version = 0x01010000,
  .software_version = 0x01010000,
  .firmware_version = 0x01010000,
  .number = 0x00000000,
  .serial_number = device_serial_number,
  .description = device_description,
};


