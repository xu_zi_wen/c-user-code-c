#include "cmd_line_app.h"
#include "gpio.h"


static void cmd_set_cycle(void);
static void cmd_set_mode(void);
static void cmd_reset(void);
static void cmd_ls(void);
static void cmd_help(void);

cmd_line_t cmd_line;
cmd_list_t cmd_list[5] = 
{
  {"setcycle", cmd_set_cycle},
  {"setmode",  cmd_set_mode},
  {"reset",    cmd_reset},
  {"ls",       cmd_ls},
  {"help",     cmd_help},
};


static void cmd_set_cycle(void)
{
  printf("setcycle %s\r\n", cmd_line.cmd_line_argv[1]);
}

static void cmd_set_mode(void)
{
  printf("setmode %s %s\r\n", cmd_line.cmd_line_argv[1], cmd_line.cmd_line_argv[2]);
}

static void cmd_reset(void)
{
  printf("reset %s %s %s\r\n", cmd_line.cmd_line_argv[1], cmd_line.cmd_line_argv[2], cmd_line.cmd_line_argv[3]);
}

static void cmd_ls(void)
{
  printf("ls\r\n");
}

static void cmd_help(void)
{
  if (strcmp(cmd_line.cmd_line_argv[1], "1") == 0)
  {
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
  }
  else
  {
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
  }
}




