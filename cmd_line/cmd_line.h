#ifndef __CMD_LINE_H__
#define __CMD_LINE_H__

#include <stdint.h>
#include <string.h>
#include <stdio.h>


// [Need modified] The max number of argument of one command
#define CMD_LINE_ARGUMENT_MAX_NUMBER            6

// [Need modified] The max length of one command. Except of 0x0D 0x0A charactors
#define CMD_LINE_MAX_LENGTH                   255

// [Need modified] String delimiter
#define CMD_LINE_STRING_DELIMITER             " "

// Cmd_Line_Getchar() Return value
#define CMD_LINE_GETCHAR_RETURN_OK              0
#define CMD_LINE_GETCHAR_RETURN_IDLE            1
#define CMD_LINE_GETCHAR_RETURN_RECEIVING       2
#define CMD_LINE_GETCHAR_RETURN_FAILED          3

// Cmd_Line_Execute() Return value
#define CMD_LINE_EXECUTE_RETURN_OK              0
#define CMD_LINE_EXECUTE_RETURN_FAILED          1

// cmd_line->cmd_line_received_state
#define CMD_LINE_RECEIVED_OK                    0
#define CMD_LINE_RECEIVED_IDLE                  1
#define CMD_LINE_RECEIVED_RECEIVING             2


// Cmd container
typedef struct 
{
  char cmd_line_buffer[CMD_LINE_MAX_LENGTH + 1];    // add last 0x00
  uint8_t cmd_line_received_index;
  int8_t cmd_line_received_state;
  uint8_t cmd_line_argc;
  char *cmd_line_argv[CMD_LINE_ARGUMENT_MAX_NUMBER];
} cmd_line_t;

// Type of cmd list
typedef struct 
{
  char *string_cmd_name;
  void (*cmd_function)(void);
} cmd_list_t;


int32_t CMD_Line_GetChar(cmd_line_t *CmdLine, char ReceivedChar);
int32_t CMD_Line_Reset(cmd_line_t *CmdLine);
int32_t CMD_Line_Execute(cmd_line_t *CmdLine, const cmd_list_t *CmdList, uint8_t CmdListNumber);

#endif




