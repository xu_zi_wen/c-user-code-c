#include "cmd_line.h"



/**
  * @brief  Get char
  * @note   NULL
  * @param  CmdLine: Pointer to an command container
  * @param  ReceivedChar: The received char for command line
  * @retval Return value of every charactor checking
  */
int32_t CMD_Line_GetChar(cmd_line_t *CmdLine, char ReceivedChar)
{
  switch (ReceivedChar)
  {
    /* 回车字符 */
    case '\r':
      CmdLine->cmd_line_received_state = CMD_LINE_RECEIVED_RECEIVING;
      return CMD_LINE_GETCHAR_RETURN_RECEIVING;
    
    /* 换行字符 */
    case '\n':
      CmdLine->cmd_line_received_state = CMD_LINE_RECEIVED_OK;
      return CMD_LINE_GETCHAR_RETURN_OK;
    
    default:
      /* 拷贝字符到结构体 */
      CmdLine->cmd_line_buffer[CmdLine->cmd_line_received_index] = ReceivedChar;
      
      /* 移动指针，准备接收下个字符 */
      CmdLine->cmd_line_received_index++;
      
      CmdLine->cmd_line_received_state = CMD_LINE_RECEIVED_RECEIVING;
      return CMD_LINE_GETCHAR_RETURN_RECEIVING;
  }
}


/**
  * @brief  Reset the command container for next receive
  * @note   NULL
  * @param  CmdLine: Pointer to an command container, it is to be reset
  * @retval 0
  */
int32_t CMD_Line_Reset(cmd_line_t *CmdLine)
{
  /* 清空结构体 */
  memset(CmdLine, 0, sizeof(cmd_line_t));
  
  CmdLine->cmd_line_received_state = CMD_LINE_RECEIVED_IDLE;
  
  return 0;
}


/**
  * @brief  Search in command list, and excute the right command func
  * @note   NULL
  * @param  CmdLine: Pointer to container filled with an command
  * @param  CmdList: Command list for searching
  * @param  CmdListNumber: The number of command list
  * @retval CMD_LINE_EXECUTE_RETURN_OK: The command function was executed
  *         CMD_LINE_EXECUTE_RETURN_FAILED: No certain command was found in the list
  */
int32_t CMD_Line_Execute(cmd_line_t *CmdLine, const cmd_list_t *CmdList, uint8_t CmdListNumber)
{
  uint32_t index = 0;
  
  /* 获取第一个子字符串 */
  CmdLine->cmd_line_argv[index] = strtok(CmdLine->cmd_line_buffer, CMD_LINE_STRING_DELIMITER);
  
  while (CmdLine->cmd_line_argv[index] != NULL) 
  {
    index++;
    
    /* 继续获取其他的子字符串 */
    CmdLine->cmd_line_argv[index] = strtok(NULL, CMD_LINE_STRING_DELIMITER);
  }
  
  for (index = 0; index < CmdListNumber; index++)
  {
    /* 判断接收的命令是否符合命令列表里的命令 */
    if (strcmp((char *)CmdList[index].string_cmd_name, CmdLine->cmd_line_argv[0]) == 0)
    {
      break;
    }
    
    /* 最后一个命令还不符合 */
    if (index == CmdListNumber - 1)
    {
      return CMD_LINE_EXECUTE_RETURN_FAILED;
    }
  }
  
  /* 执行对应命令下的函数 */
  CmdList[index].cmd_function();
  
  return CMD_LINE_EXECUTE_RETURN_OK;
}






