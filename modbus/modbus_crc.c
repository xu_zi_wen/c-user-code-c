#include "modbus_crc.h"


/**
  * @brief  Modbus CRC16校验函数
  * @param  buff: 数据缓存区
  * @param  len: 缓存区数据长度
  * @retval CRC16校验结果
  */
uint16_t MODBUS_CRC16(uint8_t *buff, uint32_t len)
{
  uint16_t wcrc = 0xFFFF; // 预置16位CRC寄存器，初值全部为1
  uint32_t i = 0, j = 0;  // 定义计数
  
  for (i = 0; i < len; i++)  // 循环计算每个数据
  {
    wcrc ^= *buff++;    // 将八位数据与crc寄存器亦或，指针地址增加，指向下个数据
  
    for (j = 0; j < 8; j++) // 循环计算数据的
    {
      if (wcrc & 0x0001)    // 判断右移出的是不是1，如果是1则与多项式进行异或。
      {
        wcrc = (wcrc >> 1) ^ 0xA001;  // 先将数据右移一位再与上面的多项式进行异或
      }
      else // 如果不是1，则直接移出
      {
        wcrc >>= 1; // 直接移出
      }
    }
		
  }
  return ((wcrc << 8) | (wcrc >> 8)); // 低八位在前，高八位在后
}



