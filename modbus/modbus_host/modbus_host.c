#include "modbus_host.h"



static uint8_t send_data[256 + 16];
static int32_t MODBUS_Host_ReturnData_Processing(MODBUS_User_Reg_t *MODBUS_Host, 
                 MODBUS_Class_t MB_class);


/**
  * @brief  Modbus主机用户寄存器注册
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @retval Modbus从机响应，参见@MODBUS_Error_t
  */
int32_t MODBUS_Host_Register(MODBUS_User_Reg_t *MODBUS_Host)
{
  int32_t modbus_ret = MODBUS_SUCCEED;
  
  modbus_ret = MODBUS_Host->modbus_init(MODBUS_Host);
  
  return modbus_ret;
}


/**
  * @brief  Modbus主机数据处理
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  MB_class: RTU还是TCP
  * @retval 从机响应，参见@MODBUS_Error_t
  */
int32_t MODBUS_Host_Data_Processing(MODBUS_User_Reg_t *MODBUS_Host, MODBUS_Class_t Class)
{
  int32_t modbus_ret = MODBUS_SUCCEED;
  uint16_t Reg_Addr, Reg_Count, Reg_Value;
  
  if (Class == MODBUS_RTU)
  {
    /* 验证CRC */
    if (MODBUS_CRC16(MODBUS_Host->rx_buffer_rtu->data, *MODBUS_Host->rx_buffer_rtu->length) != 0)
    {
      return MODBUS_CRC_ERROR;
    }
    
    /* 验证从机ID */
    if (MODBUS_Host_ID_Inspect(MODBUS_Host, MODBUS_Host->rx_buffer_rtu->data[0])
      == MODBUS_ID_INSPECT_ERROR)
    {
      return MODBUS_ID_ERROR;
    }
    
    switch (MODBUS_Host->rx_buffer_rtu->data[1])
    {
      /* 读取保持寄存器 */
      case 0x03:
        Reg_Addr = ((send_data[8] << 8) | send_data[9]);
        Reg_Count = MODBUS_Host->rx_buffer_rtu->data[2] / 2;
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        MODBUS_Host->read_hold_reg(Reg_Addr, &MODBUS_Host->rx_buffer_rtu->data[3], Reg_Count);
        break;
      
      /* 读取输入寄存器 */
      case 0x04:
        Reg_Addr = ((send_data[8] << 8) | send_data[9]);
        Reg_Count = MODBUS_Host->rx_buffer_rtu->data[2] / 2;
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        MODBUS_Host->read_input_reg(Reg_Addr, &MODBUS_Host->rx_buffer_rtu->data[3], Reg_Count);
        break;
      
      /* 写单个保持寄存器 */
      case 0x06:
        Reg_Addr = (MODBUS_Host->rx_buffer_rtu->data[2] << 8) | MODBUS_Host->rx_buffer_rtu->data[3];
        Reg_Value = (MODBUS_Host->rx_buffer_rtu->data[4] << 8) | MODBUS_Host->rx_buffer_rtu->data[5];
        if (Reg_Addr != ((send_data[8] << 8) | send_data[9]))
        {
          return MODBUS_ADDR_ERROR;
        }
        if (Reg_Value != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        break;
      
      /* 写多个保持寄存器 */
      case 0x10:
        Reg_Addr = (MODBUS_Host->rx_buffer_rtu->data[2] << 8) | MODBUS_Host->rx_buffer_rtu->data[3];
        Reg_Count = (MODBUS_Host->rx_buffer_rtu->data[4] << 8) | MODBUS_Host->rx_buffer_rtu->data[5];
        if (Reg_Addr != ((send_data[8] << 8) | send_data[9]))
        {
          return MODBUS_ADDR_ERROR;
        }
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        break;
      
      /* 自定义读文件 */
      case 0x43:
        break;
      
      /* 自定义写文件 */
      case 0x46:
        break;
      
      /* 未知的功能码 */
      default:
        modbus_ret = MODBUS_FUNC_ERROR;
        break;
    }
  }
  
  else if (Class == MODBUS_TCP)
  {
    /* 验证 */
    if ((MODBUS_Host->rx_buffer_tcp->data[2] != 0)
      || (MODBUS_Host->rx_buffer_tcp->data[3] != 0))
    {
      return MODBUS_ID_ERROR;
    }
		
		if (((MODBUS_Host->rx_buffer_tcp->data[4] << 8) | MODBUS_Host->rx_buffer_tcp->data[5])
      != *MODBUS_Host->rx_buffer_tcp->length - 6)
    {
      return MODBUS_ID_ERROR;
    }
    
    /* 验证从机ID */
    if (MODBUS_Host_ID_Inspect(MODBUS_Host, MODBUS_Host->rx_buffer_tcp->data[6])
      == MODBUS_ID_INSPECT_ERROR)
    {
      return MODBUS_ID_ERROR;
    }
    
    switch (MODBUS_Host->rx_buffer_tcp->data[7])
    {
      /* 读取保持寄存器 */
      case 0x03:
        Reg_Addr = ((send_data[8] << 8) | send_data[9]);
        Reg_Count = MODBUS_Host->rx_buffer_tcp->data[8] / 2;
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        MODBUS_Host->read_hold_reg(Reg_Addr, &MODBUS_Host->rx_buffer_tcp->data[9], Reg_Count);
        break;
      
      /* 读取输入寄存器 */
      case 0x04:
        Reg_Addr = ((send_data[8] << 8) | send_data[9]);
        Reg_Count = MODBUS_Host->rx_buffer_tcp->data[8] / 2;
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        MODBUS_Host->read_input_reg(Reg_Addr, &MODBUS_Host->rx_buffer_tcp->data[9], Reg_Count);
        break;
      
      /* 写单个保持寄存器 */
      case 0x06:
        Reg_Addr = (MODBUS_Host->rx_buffer_tcp->data[8] << 8) | MODBUS_Host->rx_buffer_tcp->data[9];
        Reg_Value = (MODBUS_Host->rx_buffer_tcp->data[10] << 8) | MODBUS_Host->rx_buffer_tcp->data[11];
        if (Reg_Addr != ((send_data[8] << 8) | send_data[9]))
        {
          return MODBUS_ADDR_ERROR;
        }
        if (Reg_Value != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        break;
      
      /* 写多个保持寄存器 */
      case 0x10:
        Reg_Addr = (MODBUS_Host->rx_buffer_tcp->data[8] << 8) | MODBUS_Host->rx_buffer_tcp->data[9];
        Reg_Count = (MODBUS_Host->rx_buffer_tcp->data[10] << 8) | MODBUS_Host->rx_buffer_tcp->data[11];
        if (Reg_Addr != ((send_data[8] << 8) | send_data[9]))
        {
          return MODBUS_ADDR_ERROR;
        }
        if (Reg_Count != ((send_data[10] << 8) | send_data[11]))
        {
          return MODBUS_DATA_ERROR;
        }
        break;
      
      /* 自定义读文件 */
      case 0x43:
        break;
      
      /* 自定义写文件 */
      case 0x46:
        break;
      
      /* 未知的功能码 */
      default:
        modbus_ret = MODBUS_FUNC_ERROR;
        break;
    }
  }
  
  return modbus_ret;
}



/**
  * @brief  Modbus验证从机ID
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  Id：需要验证的从机ID
  * @retval Modbus从机响应，参见@MODBUS_ID_Inpect_t
  */
MODBUS_ID_Inpect_t MODBUS_Host_ID_Inspect(MODBUS_User_Reg_t *MODBUS_Host, uint8_t Id)
{
  uint8_t dev_id = *MODBUS_Host->modbus_device_id;
  if (Id == dev_id)
  {
    return MODBUS_ID_INSPECT_NATIVE;
  }
  else if (Id == 0x00)
  {
    return MODBUS_ID_INSPECT_ADVER;
  }
  else
  {
    return MODBUS_ID_INSPECT_ERROR;
  }
}


/**
  * @brief  Modbus主机下发读输入寄存器的指令
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  Class: TCP还是RTU
  * @param  Reg_Addr: 寄存器地址
  * @param  Reg_Count: 寄存器数量
  * @retval Modbus从机响应，参见@MODBUS_Error_t
  */
int32_t MODBUS_Host_Read_InputReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count)
{
  uint16_t crc16;
  int32_t res;
  
  send_data[6] = *MODBUS_Host->modbus_device_id;
  send_data[7] = 0x04;
  send_data[8] = (uint8_t)(Reg_Addr >> 8);
  send_data[9] = (uint8_t)(Reg_Addr >> 0);
  send_data[10] = (uint8_t)(Reg_Count >> 8);
  send_data[11] = (uint8_t)(Reg_Count >> 0);
  
  crc16 = MODBUS_CRC16(&send_data[6], 6); // 计算前6个字节的CRC值
  send_data[12] = (uint8_t)(crc16 >> 8);  // 填充CRC值高8位
  send_data[13] = (uint8_t)(crc16 >> 0);  // 填充CRC值低8位
  
  /* Modbus-TCP字段，RTU不需要 */
  send_data[0] = 0x00;
  send_data[1] = 0x00;
  send_data[2] = 0x00;
  send_data[3] = 0x00;
  send_data[4] = 0x00;
  send_data[5] = 0x06;
  
  if (Class == MODBUS_RTU)
  {
    memcpy(MODBUS_Host->tx_buffer_rtu->data, send_data, 12 + 2);
    res = MODBUS_Host->send_rtu(MODBUS_Host->send_rtu_com,
            &MODBUS_Host->tx_buffer_rtu->data[6], 8);
  }
  else if (Class == MODBUS_TCP)
  {
    memcpy(MODBUS_Host->tx_buffer_tcp->data, send_data, 12 + 2);
    res = MODBUS_Host->send_tcp(MODBUS_Host->send_tcp_com,
            &MODBUS_Host->tx_buffer_tcp->data[0], 12);
  }
  
  res = MODBUS_Host_ReturnData_Processing(MODBUS_Host, Class);
  
  return res;
}


/**
  * @brief  Modbus主机下发读保持寄存器的指令
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  Class: TCP还是RTU
  * @param  Reg_Addr: 寄存器地址
  * @param  Reg_Count: 寄存器数量
  * @retval Modbus从机响应，参见@MODBUS_Error_t
  */
int32_t MODBUS_Host_Read_HoldReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count)
{
  uint16_t crc16;
  int32_t res;
  
  send_data[6] = *MODBUS_Host->modbus_device_id;
  send_data[7] = 0x03;
  send_data[8] = (uint8_t)(Reg_Addr >> 8);
  send_data[9] = (uint8_t)(Reg_Addr >> 0);
  send_data[10] = (uint8_t)(Reg_Count >> 8);
  send_data[11] = (uint8_t)(Reg_Count >> 0);
  
  crc16 = MODBUS_CRC16(&send_data[6], 6); // 计算前6个字节的CRC值
  send_data[12] = (uint8_t)(crc16 >> 8);  // 填充CRC值高8位
  send_data[13] = (uint8_t)(crc16 >> 0);  // 填充CRC值低8位
  
  /* Modbus-TCP字段，RTU不需要 */
  send_data[0] = 0x00;
  send_data[1] = 0x00;
  send_data[2] = 0x00;
  send_data[3] = 0x00;
  send_data[4] = 0x00;
  send_data[5] = 0x06;
  
  if (Class == MODBUS_RTU)
  {
    memcpy(MODBUS_Host->tx_buffer_rtu->data, send_data, 12 + 2);
    res = MODBUS_Host->send_rtu(MODBUS_Host->send_rtu_com,
            &MODBUS_Host->tx_buffer_rtu->data[6], 8);
  }
  else if (Class == MODBUS_TCP)
  {
    memcpy(MODBUS_Host->tx_buffer_tcp->data, send_data, 12 + 2);
    res = MODBUS_Host->send_tcp(MODBUS_Host->send_tcp_com,
            &MODBUS_Host->tx_buffer_tcp->data[0], 12);
  }
  
  res = MODBUS_Host_ReturnData_Processing(MODBUS_Host, Class);
  
  return res;
}


/**
  * @brief  Modbus主机下发写保持寄存器的指令
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  Class: TCP还是RTU
  * @param  Reg_Addr: 寄存器地址
  * @param  Reg_Count: 寄存器数量
  * @param  Data: 要写入的寄存器数据
  * @retval Modbus从机响应，参见@MODBUS_Error_t
  */
int32_t MODBUS_Host_Write_HoldReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count, uint8_t *Data)
{
  uint16_t crc16;
  int32_t res;
  uint16_t i;
  uint16_t length = 0;
  uint16_t data_length;
  
  /* 写单个保持寄存器 */
  if (Reg_Count == 1)
  {
    send_data[6] = *MODBUS_Host->modbus_device_id;
    send_data[7] = 0x06;
    send_data[8] = (uint8_t)(Reg_Addr >> 8);
    send_data[9] = (uint8_t)(Reg_Addr >> 0);
    send_data[10] = Data[0];
    send_data[11] = Data[1];
    
    length = 6;
    
    /* 计算CRC并填充 */
    crc16 = MODBUS_CRC16(&send_data[6], 6);
    send_data[12] = (uint8_t)(crc16 >> 8);
    send_data[13] = (uint8_t)(crc16 >> 0);
    
    /* Modbus-TCP字段，RTU不需要 */
    send_data[0] = 0x00;
    send_data[1] = 0x00;
    send_data[2] = 0x00;
    send_data[3] = 0x00;
    send_data[4] = 0x00;
    send_data[5] = 0x06;
  }
  
  /* 写多个保持寄存器 */
  else if (Reg_Count > 1)
  {
    send_data[6] = *MODBUS_Host->modbus_device_id;
    send_data[7] = 0x10;
    send_data[8] = (uint8_t)(Reg_Addr >> 8);
    send_data[9] = (uint8_t)(Reg_Addr >> 0);
    send_data[10] = (uint8_t)(Reg_Count >> 8);
    send_data[11] = (uint8_t)(Reg_Count >> 0);
    
    /* 写多个保持寄存器的数据长度 */
    data_length = send_data[11] * 2;
    
    send_data[12] = data_length;
    
    /* 填充数据 */
    for (i = 0; i < data_length; i++)
    {
      send_data[13 + i] = Data[i];
    }
    
    /* 计算要做CRC的数据长度 */
    length = 7 + data_length;
    
    /* 计算CRC并填充 */
    crc16 = MODBUS_CRC16(&send_data[6], length);
    send_data[13 + data_length + 0] = (uint8_t)(crc16 >> 8);
    send_data[13 + data_length + 1] = (uint8_t)(crc16 >> 0);
    
    /* Modbus-TCP字段，RTU不需要 */
    send_data[0] = 0x00;
    send_data[1] = 0x00;
    send_data[2] = 0x00;
    send_data[3] = 0x00;
    send_data[4] = (uint8_t)(length >> 8);
    send_data[5] = (uint8_t)(length >> 0);
  }
  
  /* 啥也不是 */
  else
  {
    /* 史诗级错误 */
    res = 0x0E;
  }
  
  if (Class == MODBUS_RTU)
  {
    memcpy(MODBUS_Host->tx_buffer_rtu->data, send_data, 6 + length + 2);
    res = MODBUS_Host->send_rtu(MODBUS_Host->send_rtu_com,
            &MODBUS_Host->tx_buffer_rtu->data[6], 2 + length);
  }
  else if (Class == MODBUS_TCP)
  {
    memcpy(MODBUS_Host->tx_buffer_tcp->data, send_data, 6 + length + 2);
    res = MODBUS_Host->send_tcp(MODBUS_Host->send_tcp_com,
            &MODBUS_Host->tx_buffer_tcp->data[0], 6 + length);
  }
  else
  {
    /* 史诗级错误 */
    res = 0x0E;
  }
  
  res = MODBUS_Host_ReturnData_Processing(MODBUS_Host, Class);
  
  return res;
}


/**
  * @brief  Modbus主机下发指令后等待从机响应
  * @note   1个寄存器占2Byte
  * @param  MODBUS_Host: Host对象
  * @param  Class: TCP还是RTU
  * @retval Modbus从机响应，参见@MODBUS_Error_t
  */
static int32_t MODBUS_Host_ReturnData_Processing(MODBUS_User_Reg_t *MODBUS_Host, 
                 MODBUS_Class_t MB_class)
{
  uint32_t count_ms = 0;
  int32_t res = 0;
  
  if (MB_class == MODBUS_RTU)
  {
    /* 等待从机发来数据 */
    while (*MODBUS_Host->rx_buffer_rtu->status != MODBUS_TRANSFER_STATUS_END)
    {
      MODBUS_Host->modbus_delay(MODBUS_Host, 1);
      count_ms++;
      if (count_ms > *MODBUS_Host->modbus_timeout)
      {
        /* 超时跳出 */
        res = 0x0E;
        break;
      }
    }
    
    if (res == 0)
    {
      MODBUS_Host_Data_Processing(MODBUS_Host, MODBUS_RTU);
    }
    
    memset(MODBUS_Host->rx_buffer_rtu->data, 0, *MODBUS_Host->rx_buffer_rtu->length);
    *MODBUS_Host->rx_buffer_rtu->status = MODBUS_TRANSFER_STATUS_IDLE;
    *MODBUS_Host->rx_buffer_rtu->length = 0;
  }
	
  else if (MB_class == MODBUS_TCP)
  {
    /* 等待从机发来数据 */
    while (*MODBUS_Host->rx_buffer_tcp->status != MODBUS_TRANSFER_STATUS_END)
    {
      MODBUS_Host->modbus_delay(MODBUS_Host, 1);
      count_ms++;
      if (count_ms > *MODBUS_Host->modbus_timeout)
      {
        /* 超时跳出 */
        res = 0x0E;
        break;
      }
    }
    
    if (res == 0)
    {
      MODBUS_Host_Data_Processing(MODBUS_Host, MODBUS_TCP);
    }
    
    memset(MODBUS_Host->rx_buffer_tcp->data, 0, *MODBUS_Host->rx_buffer_tcp->length);
    *MODBUS_Host->rx_buffer_tcp->status = MODBUS_TRANSFER_STATUS_IDLE;
    *MODBUS_Host->rx_buffer_tcp->length = 0;
  }
  
  return res;
}


