#ifndef __MODBUS_HOST_H__
#define __MODBUS_HOST_H__

#include "modbus_def.h"
#include "modbus_crc.h"


int32_t MODBUS_Host_Register(MODBUS_User_Reg_t *MODBUS_Host);

int32_t MODBUS_Host_Data_Processing(MODBUS_User_Reg_t *MODBUS_Host, MODBUS_Class_t Class);
MODBUS_ID_Inpect_t MODBUS_Host_ID_Inspect(MODBUS_User_Reg_t *MODBUS_Host, uint8_t Id);

int32_t MODBUS_Host_Read_InputReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count);
int32_t MODBUS_Host_Read_HoldReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count);
int32_t MODBUS_Host_Write_HoldReg(MODBUS_User_Reg_t *MODBUS_Host,
          MODBUS_Class_t Class, uint16_t Reg_Addr, uint16_t Reg_Count, uint8_t *Data);

#endif


