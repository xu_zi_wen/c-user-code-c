#include "modbus_host_socket.h"
#include <sys/socket.h> 


extern uint8_t test_data[2048];
extern uint16_t test_data_size;
extern int32_t test_status;
extern int accept_fd;

/********************************** define **************************************/
static int32_t host_init(void *handle);
static int32_t host_read_input_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count);
static int32_t host_read_hold_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count);
static int32_t host_write_hold_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count);
static int32_t host_send_data(void *com_handle, uint8_t *data, uint16_t length);
static int32_t host_delay(void *handle, uint32_t ms);


static MODBUS_Data_Buffer_t tx_buffer = 
{
  test_data,
  &test_data_size,
  (int32_t *)&test_status,
};

static MODBUS_Data_Buffer_t rx_buffer = 
{
  test_data,
  &test_data_size,
  (int32_t *)&test_status,
};

static MODBUS_Reg_Count_t input_reg;
static MODBUS_Reg_Count_t hold_reg;

static uint16_t socket_reg_start_addr = 0x0000;
static uint16_t socket_reg_count = 0x0100;

static uint8_t device_id = 0x02;
static uint32_t timeout = 100;



MODBUS_User_Reg_t modbus_host_socket = 
{
  &modbus_host_socket,
  host_init,
};

static int32_t host_init(void *handle)
{
  MODBUS_User_Reg_t *host_handle = (MODBUS_User_Reg_t *)handle;
  
  host_handle->modbus_delay = host_delay;
  host_handle->read_input_reg = host_read_input_reg;
  host_handle->read_hold_reg = host_read_hold_reg;
  host_handle->write_hold_reg = host_write_hold_reg;
  
  host_handle->send_rtu = host_send_data;
  host_handle->send_rtu_com = &accept_fd;
  host_handle->tx_buffer_rtu = &tx_buffer;
  host_handle->rx_buffer_rtu = &rx_buffer;
  
  host_handle->send_tcp = host_send_data;
  host_handle->send_tcp_com = &accept_fd;
  host_handle->tx_buffer_tcp = &tx_buffer;
  host_handle->rx_buffer_tcp = &rx_buffer;
  
  host_handle->modbus_device_id = &device_id;
  host_handle->modbus_timeout = &timeout;

  input_reg.start_addr = &socket_reg_start_addr;
  input_reg.count = &socket_reg_count;
  host_handle->input_reg = &input_reg;

  hold_reg.start_addr = &socket_reg_start_addr;
  hold_reg.count = &socket_reg_count;
  host_handle->hold_reg = &hold_reg;
  
  return 0;
}

static int32_t host_delay(void *handle, uint32_t ms)
{
  return 0;
}

static int32_t host_send_data(void *com_handle, uint8_t *data, uint16_t length)
{
  int *test_com = (int *)com_handle;

  return send(*test_com, data, length, 0);
}


static int32_t host_read_input_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count)
{
  
  return 0;
}


static int32_t host_read_hold_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count)
{
  
  return 0;
}


static int32_t host_write_hold_reg(uint16_t reg_addr, uint8_t *reg_data, uint8_t reg_count)
{
  return 0;
}











