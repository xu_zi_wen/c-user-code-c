#ifndef __MODBUS_DEF_H__
#define __MODBUS_DEF_H__

#include <stdint.h>
#include <string.h>
#include <stdio.h>


typedef enum __MODBUS_Error_t
{
  MODBUS_SUCCEED      = 0,
  MODBUS_FUNC_ERROR   = 1,
  MODBUS_ADDR_ERROR   = 2,
  MODBUS_DATA_ERROR   = 3,
  MODBUS_ID_ERROR     = 4,
  MODBUS_CRC_ERROR    = 5,
} MODBUS_Error_t;


typedef enum __MODBUS_Class_t
{
  MODBUS_RTU     = 0,
  MODBUS_ASCII   = 1,
  MODBUS_TCP     = 2,
} MODBUS_Class_t;


typedef enum __MODBUS_ID_Inpect_t
{
  MODBUS_ID_INSPECT_NATIVE  = 0,
  MODBUS_ID_INSPECT_ADVER   = 1,
  MODBUS_ID_INSPECT_ERROR   = 2,
} MODBUS_ID_Inpect_t;


typedef enum __MODBUS_Transfer_Status_t
{
  MODBUS_TRANSFER_STATUS_IDLE   = 0,
  MODBUS_TRANSFER_STATUS_START  = 1,
  MODBUS_TRANSFER_STATUS_END    = 2,
} MODBUS_Transfer_Status_t;


typedef struct __MODBUS_Bit_Count_t
{
  uint16_t *start_addr;
  uint16_t *count;
} MODBUS_Bit_Count_t;

typedef struct __MODBUS_Reg_Count_t
{
  uint16_t *start_addr;
  uint16_t *count;
} MODBUS_Reg_Count_t;


typedef struct __MODBUS_Data_Buffer_t
{
  uint8_t *data;
  uint16_t *length;
  int32_t *status;  
} MODBUS_Data_Buffer_t;


typedef int32_t (*modbus_init_t)(void *);
typedef int32_t (*modbus_delay_t)(void *, uint32_t);
typedef int32_t (*modbus_transfer_t)(void *, uint8_t *, uint16_t);
typedef int32_t (*modbus_rw_bit_t)(uint16_t, uint8_t *, uint16_t);
typedef int32_t (*modbus_rw_reg_t)(uint16_t, uint8_t *, uint8_t);
typedef int32_t (*modbus_rw_file_t)(uint16_t, uint32_t, uint8_t *, uint16_t);
typedef struct __MODBUS_User_Reg_t
{
  void *handle;
  modbus_init_t modbus_init;
  modbus_delay_t modbus_delay;
  modbus_rw_bit_t read_coil_bit;
  modbus_rw_bit_t write_coil_bit;
  modbus_rw_bit_t read_discrete_bit;
  modbus_rw_reg_t read_input_reg;
  modbus_rw_reg_t read_hold_reg;
  modbus_rw_reg_t write_hold_reg;
  modbus_rw_file_t read_user_file;
  modbus_rw_file_t write_user_file;
  
  modbus_transfer_t send_rtu;
  void *send_rtu_com;
  MODBUS_Data_Buffer_t *tx_buffer_rtu;
  MODBUS_Data_Buffer_t *rx_buffer_rtu;
  
  modbus_transfer_t send_tcp;
  void *send_tcp_com;
  MODBUS_Data_Buffer_t *tx_buffer_tcp;
  MODBUS_Data_Buffer_t *rx_buffer_tcp;

  uint8_t *modbus_device_id;
  uint32_t *modbus_timeout;
  
  MODBUS_Bit_Count_t *coil_bit;
  MODBUS_Bit_Count_t *discrete_bit;
  MODBUS_Reg_Count_t *input_reg;
  MODBUS_Reg_Count_t *hold_reg;
} MODBUS_User_Reg_t;


#endif


