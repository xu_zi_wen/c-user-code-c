#ifndef __MODBUS_DEVICE_SOCKET_H__
#define __MODBUS_DEVICE_SOCKET_H__

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "modbus_device.h"


#define PRODUCT_ID              0x00000135  // ��ƷID
#define HARDWARE_VERSION        0x01020000  // Ӳ���汾
#define FIRMWARE_VERSION        0x01020003  // �̼��汾

#define PRODUCT_SERIAL_NUMBER   "2024Q3N00001"  // ���к�
#define PRODUCT_MODEL           "STM32H750XB"  // ��Ʒ�ͺ�


/*********************************** Modbus�Ĵ��� ************************************/
/*      �Ĵ�������                      �Ĵ�����ַ    �Ĵ�������                     */
#define D1_HR_PRODUCT_ID                  0x0000  // ��Ʒid��4�ֽڣ�u32
#define D1_HR_FIRMWARE_VERSION            0x0002  // �̼��汾��4�ֽڣ�u32
#define D1_HR_HARDWARE_VERSION            0x0004  // Ӳ���汾��4�ֽڣ�u32
#define D1_HR_PRODUCT_SN                  0x0006  // ��Ʒ��ţ�32�ֽڣ��ַ���
#define D1_HR_PRODUCT_MODEL               0x0016  // ��Ʒ�ͺţ�64�ֽڣ��ַ���


extern uint8_t modbus_d1_buff[];
extern MODBUS_User_Reg_t modbus_device_socket;


#endif


