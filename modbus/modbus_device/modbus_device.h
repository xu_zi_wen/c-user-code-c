#ifndef __MODBUS_DEVICE_H__
#define __MODBUS_DEVICE_H__

#include "modbus_def.h"
#include "modbus_crc.h"


int32_t MODBUS_Device_Register(MODBUS_User_Reg_t *MODBUS_Device);
int32_t MODBUS_Device_Data_Processing(MODBUS_User_Reg_t *MODBUS_Device, MODBUS_Class_t Class);
MODBUS_ID_Inpect_t MODBUS_Device_ID_Inspect(MODBUS_User_Reg_t *MODBUS_Device, uint8_t Id);

int32_t MODBUS_Device_Read_DiscreteBit(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint16_t Size);
int32_t MODBUS_Device_Read_CoilBit(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint16_t Size);
int32_t MODBUS_Device_Read_InputReg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);
int32_t MODBUS_Device_Read_HoldReg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);
int32_t MODBUS_Device_Write_HoldReg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);
int32_t MODBUS_Device_Write_MultipleHoldReg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);
int32_t MODBUS_Device_Read_File(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);
int32_t MODBUS_Device_Write_File(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data, uint8_t Size);

int32_t MODBUS_Device_Return_Error_Code(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func, uint8_t Error);
int32_t MODBUS_Device_Return_Read_Bit(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func, uint8_t *Data, uint16_t Len);
int32_t MODBUS_Device_Return_Write_Bit(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func, uint8_t *Data, uint16_t Len);
int32_t MODBUS_Device_Return_Read_Reg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func, uint8_t *Data, uint16_t Len);
int32_t MODBUS_Device_Return_Write_Reg(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t *Data);
int32_t MODBUS_Device_Return_Read_File(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func,
          uint16_t File_Num, uint32_t Pack_Num, uint8_t *Data, uint16_t Pack_Len);
int32_t MODBUS_Device_Return_Write_File(MODBUS_User_Reg_t *MODBUS_Device,
          MODBUS_Class_t Class, uint8_t Func,
          uint16_t File_Num, uint32_t Pack_Num, uint8_t *Data, uint16_t Pack_Len);

int32_t MODBUS_Device_DiscreteBit_Addr_Check(MODBUS_User_Reg_t *MODBUS_Device,
          uint32_t Addr, uint16_t Bit_Count);
int32_t MODBUS_Device_CoilBit_Addr_Check(MODBUS_User_Reg_t *MODBUS_Device,
          uint32_t Addr, uint16_t Bit_Count);
int32_t MODBUS_Device_InputReg_Addr_Check(MODBUS_User_Reg_t *MODBUS_Device,
          uint32_t Addr, uint16_t Reg_Count);
int32_t MODBUS_Device_HoldReg_Addr_Check(MODBUS_User_Reg_t *MODBUS_Device,
          uint32_t Addr, uint16_t Reg_Count);
int32_t MODBUS_Device_File_Check(MODBUS_User_Reg_t *MODBUS_Device,
          uint32_t File_Num, uint32_t Pack_Num, uint16_t Pack_Len);

#endif


