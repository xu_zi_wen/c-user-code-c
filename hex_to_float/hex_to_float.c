#include "hex_to_float.h"


/**
  * @brief  uint8_t型转换为double型
  * @note   无
  * @param  Hex: uint8_t型数组
  * @param  Double: double型数组
  * @param  Count: uint8_t型数组内元素个数
  * @retval 0
  */
int32_t Hex_To_Double(uint8_t *Hex, double *Double, uint8_t Count)
{
  uint8_t double_index = 0, i = 0; 
  uint32_t temp_u32[2];
  uint64_t temp_u64;
  double *p;
  
  for (i = 0; i < Count; i++)
  {
    temp_u32[0] = Hex[double_index + 0] << 24 
                | Hex[double_index + 1] << 16
                | Hex[double_index + 2] << 8
                | Hex[double_index + 3] << 0;
    temp_u32[1] = Hex[double_index + 4] << 24
                | Hex[double_index + 5] << 16
                | Hex[double_index + 6] << 8
                | Hex[double_index + 7] << 0;
    
    temp_u64 = (uint64_t)temp_u32[0] << 32;
    temp_u64 += temp_u32[1];
    
    p = (double *)&temp_u64;
    Double[i] = *p;

    double_index += 8;
  }
  
  return 0;
}



/**
  * @brief  double型转换为uint8_t型
  * @note   uint8_t数组高位在前低位在后
  * @param  Double: double型数组
  * @param  Hex: uint8_t型数组
  * @param  Count: double型数组内元素个数
  * @retval 0
  */
int32_t Double_To_Hex(double *Double, uint8_t *Hex, uint8_t Count)
{
  uint8_t hex_index = 0, i = 0;
  double temp_lf;
  uint64_t *p;
  
  for (i = 0; i < Count; i++)
  {
    temp_lf = Double[i];
    p = (uint64_t *)&temp_lf;
    
    Hex[hex_index + 0] = (*p) >> 56;
    Hex[hex_index + 1] = (*p) >> 48;
    Hex[hex_index + 2] = (*p) >> 40;
    Hex[hex_index + 3] = (*p) >> 32;
    Hex[hex_index + 4] = (*p) >> 24;
    Hex[hex_index + 5] = (*p) >> 16;
    Hex[hex_index + 6] = (*p) >> 8;
    Hex[hex_index + 7] = (*p) >> 0;
    
    hex_index += 8;
  }
  
  return 0;
}



/**
  * @brief  uint8_t型转换为float型
  * @note   无
  * @param  Hex: uint8_t型数组
  * @param  Float: float型数组
  * @param  Count: uint8_t型数组内元素个数
  * @retval 0
  */
int32_t Hex_To_Float(uint8_t *Hex, float *Float, uint8_t Count)
{  
  uint8_t float_index = 0, i = 0;
  uint32_t temp_u32;
  float *p;

  for (i = 0; i < Count; i++)
  {
    temp_u32 = Hex[float_index + 0] << 24
             | Hex[float_index + 1] << 16
             | Hex[float_index + 2] << 8
             | Hex[float_index + 3] << 0;
    
    p = (float *)&temp_u32;
    Float[i] = *p;
    
    float_index += 4;
	}
  
  return 0;
}


/**
  * @brief  float型转换为uint8_t型
  * @note   uint8_t数组高位在前低位在后
  * @param  Float: float型数组
  * @param  Hex: uint8_t型数组
  * @param  Count: float型数组内元素个数
  * @retval 0
  */
int32_t Float_To_Hex(float *Float, uint8_t *Hex, uint8_t Count)
{  
  uint8_t hex_index = 0, i = 0;
  float temp_f;
  uint32_t *p;
  
  for (i = 0; i < Count; i++)
  {
    temp_f = Float[i];
    p = (uint32_t *)&temp_f;

    Hex[hex_index + 0] = (*p) >> 24;
    Hex[hex_index + 1] = (*p) >> 16;
    Hex[hex_index + 2] = (*p) >> 8;
    Hex[hex_index + 3] = (*p) >> 0;
    
    hex_index += 4;
  }	
  
  return 0;
} 




