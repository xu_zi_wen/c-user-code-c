#ifndef __HEX_TO_FLOAT_H__
#define __HEX_TO_FLOAT_H__

#include <stdint.h>


int32_t Hex_To_Double(uint8_t *Hex, double *Double, uint8_t Count);
int32_t Double_To_Hex(double *Double, uint8_t *Hex, uint8_t Count);
int32_t Hex_To_Float(uint8_t *Hex, float *Float, uint8_t Count);
int32_t Float_To_Hex(float *Float, uint8_t *Hex, uint8_t Count);


#endif


